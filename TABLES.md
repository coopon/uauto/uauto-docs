# SQL Tables for uauto
## List
- Roles - Different roles to govern access for users
- Users - Users base table
- Stands - 
- Passengers
- Vehicles
- Bank Info
- Prices
- Trips
- Fare

## Assumptions
- All datatypes are for postgresql

# roles

| Name        | Datatype     |
| ---         | ---          |
| id          | SERIAL       |
| name        | VARCHAR(255) |
| description | VARCHAR(255) |

# users

| Name       | Datatype                 |
| ---        | ---                      |
| phone      | VARCHAR(255) PRIMARY KEY |
| email      | VARCHAR(255)             |
| name       | VARCHAR(255)             |
| password   | VARCHAR(255)             |
| role       | REFERENCES roles(name)   |
| created_at | TIMESTAMP                |
| deleted_at | TIMESTAMP                |
| deleted    | BOOLEAN                  |

# Stands

| Name        | Datatype     |
| ---         | ---          |
| name        | VARCHAR(255) |
| geolocation | VARCHAR(255) |

# Passengers

| Name        | Datatype     |
| ---         | ---          |
| user        | FOREIGN KEY  |
| address     | VARCHAR(255) |
| geolocation |              |

# Vehicles

| Name        | Datatype                 |
| ---         | ---                      |
| driver      | REFERENCES users (phone) |
| reg_no      | VARCHAR(20)              |
| chasis_no   | VARCHAR(100)             |
| engine_no   | VARCHAR(100)             |
| own_vehicle | BOOLEAN                  |


# Bank Info

| Name    | Datatype                |
| ---     | ---                     |
| user    | REFERENCES users(phone) |
| upi     | VARCHAR(255)            |
| qr_code | VARCHAR(255)            |
| acc_no  | VARCHAR(255)            |
|         |                         |

# Prices

| Name        | Datatype                |
| ---         | ---                     |
| commodity   | VARCHAR(255)            |
| rate        |                         |
| quantity    |                         |
| date        | DATE                    |
| location    |                         |
| contributor | REFERENCES users(phone) |
|             |                         |

# Trips

| Name      | Datatype                |
| ---       | ---                     |
| date      | DATE                    |
| driver    | REFERENCES users(phone) |
| passenger | REFERENCES users(phone) |
|           |                         |

# Fare 

| Name     | Datatype |
| ---      | ---      |
| base     |          |
| km       |          |
| date     |          |
| location |          |
| night    |          |
