# Roadmap 
## Overview
- Stage 1 - Show only nearest auto / taxi stand drivers phone numbers from current location. No fare calculation. UPI details present.
- Stage 2 - Show offline map to search destination, routing. Fare estimation for the intended rides. Ride booking is still via phone calls. 
- Stage 3 - Book rides fully online, fare calculation for all rides, ride details are recorded online but no real time communication of driver or passenger location.
- Stage 4 - Ride data is recorded on the device and submitted as a file. 
- Stage 5 - Real time communication of ride location details. 

# Stage 1 
Show only nearest auto / taxi stand drivers phone numbers from current location. No fare calculation. UPI details present.

## Questions
- login UI/UX, API, Backend ?.
- Region Constrained Map ?.
- Search nearest vehicle (auto, driver) in nearest stand ?.
- Confirmation from passenger, Message/Call to corresponding driver ? .
- What will be the user flow E2E at this stage? 
- How do we handle driver registrations?
- What is the content for the landing page?
- How to know ride is being conducted?
- What more info can we gain?
- When to show UPI & personal details of the driver?
- How prevent misuse both driver and passenger? We should manage field in db for mark as spam.
- Redirect to thirdparty maps by link from our app is right?

## Critical data requirements
- Stand data
  - Geolocation
  - Name
  
- Driver data
  - Name
  - Stand 
  - Phone number
  - Timings?

## Web
- uauto.in

## API
- Registration
  - Driver
  - Passenger 
- Availability 
- Information
  - Stand
  - Drivers

## Driver app
- Minimum one app for every stand 
- Mark availability / attendance of every driver time to time

## Passenger app
- Check current location
- Show nearest stand details
  - Show drivers as per availability
  - Make call 
